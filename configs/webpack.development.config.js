const { resolve } = require('path');

module.exports = {
  entry: {
    'marked-in-html': './src/index.js'
  } ,
  output: {
    path: resolve(__dirname ,'../dist') ,
    filename: '[name].js'
  } ,
  externals: {
    marked: 'marked' ,
    'highlight.js': 'hljs'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/ ,
        use: [{
          loader: 'babel-loader' ,
          query: {
            presets: ['es2015']
          }
        }]
      }
    ]
  } ,
  resolve: {
    extensions: ['.js', '.jsx'] ,
    modules: ['src' ,'node_modules']
  } ,
  plugins: []
};
